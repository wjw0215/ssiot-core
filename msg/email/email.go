package email

import (
	"crypto/tls"
	"errors"
	"fmt"
	"gitee.com/sansaniot/ssiot-core/msg/msgtype"
	"strings"
	"time"

	"gopkg.in/gomail.v2"
)

type EmailParam struct {
	To      []string
	Data    string
	Test    bool
	MsgCode string //see msttype.sms_type
}

type EmailConfig struct {
	Host     string
	Port     int
	Username string
	Password string
}

var emailConfig *EmailConfig

func InitConfig(conf *EmailConfig) error {
	if conf == nil {
		return errors.New("邮件服务未配置。")
	} else {
		emailConfig = conf
	}
	return nil
}

// SendEmailByData /*
/*
邮件发送示例:
email.SendEmailByData(
		subject:      "邮件主题",
		content:    "邮件内容",
		to:      ["hwu@hongdian.com","hwu2@hongdian.com"],
	)
*/
func SendEmailByData(subject, content string, to []string) error {
	if emailConfig == nil {
		if err := InitConfig(nil); err != nil {
			return err
		}
	}
	m := gomail.NewMessage()

	m.SetHeader("From", fmt.Sprintf("hongdian_support<%s>", emailConfig.Username)) // 发件人
	m.SetHeader("To", to...)                                                       // 收件人，可以多个收件人，但必须使用相同的 SMTP 连接

	m.SetHeader("Subject", subject) // 邮件主题

	m.SetBody("text/html", content)

	d := gomail.NewDialer(
		emailConfig.Host,
		emailConfig.Port,
		emailConfig.Username,
		emailConfig.Password,
	)
	// 关闭SSL协议认证
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

// SendEmailByType /*
/*
邮件发送示例:
email.SendEmailByType(email.EmailParam{
		To:      "hwu@hongdian.com",
		Data:    "8888",
	},msgtype.MsgType{Msg:'',Name:''})
*/
func SendEmailByType(param EmailParam, msgType msgtype.MsgType) error {
	if emailConfig == nil {
		if err := InitConfig(nil); err != nil {
			return err
		}
	}
	m := gomail.NewMessage()

	msgContent := msgType.Msg
	msgContent = strings.ReplaceAll(msgContent, "${code}", param.Data)
	msgContent = strings.ReplaceAll(msgContent, "${username}", param.Data)

	m.SetHeader("From", fmt.Sprintf("walle_support<%s>", emailConfig.Username)) // 发件人
	m.SetHeader("To", param.To...)                                              // 收件人，可以多个收件人，但必须使用相同的 SMTP 连接
	m.SetHeader("Subject", msgType.Name)                                        // 邮件主题

	m.SetBody("text/html", msgContent)

	d := gomail.NewDialer(
		emailConfig.Host,
		emailConfig.Port,
		emailConfig.Username,
		emailConfig.Password,
	)
	// 关闭SSL协议认证
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

// SendEmail /*
/*
邮件发送示例:
email.SendEmail(email.EmailParam{
		To:      "hwu@hongdian.com",
		Data:    "8888",
		MsgCode: msgtype.RegCode.MsgCode,
	})
*/
func SendEmail(param EmailParam) error {
	if emailConfig == nil {
		if err := InitConfig(nil); err != nil {
			return err
		}
	}
	m := gomail.NewMessage()
	var msgType msgtype.MsgType
	var msgContent string
	if param.Test {
		msgType = msgtype.TestMsg
		msgContent = msgType.Msg
		msgContent = strings.ReplaceAll(msgContent, "${dataTime}", time.Now().Format("2006-01-02 15:04:05"))
	} else {
		msgType = msgtype.GetMessage(param.MsgCode)
		msgContent = msgType.Msg
		msgContent = strings.ReplaceAll(msgContent, "${code}", param.Data)
		msgContent = strings.ReplaceAll(msgContent, "${username}", param.Data)
	}

	m.SetHeader("From", fmt.Sprintf("walle_support<%s>", emailConfig.Username)) // 发件人
	m.SetHeader("To", param.To...)                                              // 收件人，可以多个收件人，但必须使用相同的 SMTP 连接
	m.SetHeader("Subject", msgType.Name)                                        // 邮件主题

	m.SetBody("text/html", msgContent)

	d := gomail.NewDialer(
		emailConfig.Host,
		emailConfig.Port,
		emailConfig.Username,
		emailConfig.Password,
	)
	// 关闭SSL协议认证
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}
