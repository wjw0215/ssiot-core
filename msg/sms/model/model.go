package model

// SMSCustParam 自定义短信参数
type SMSCustParam struct {
	Phone []string          `common:"手机号"`
	Param map[string]string `common:"短信参数（占位符的key与value）"`
	Code  string            `common:"短信模板Code"`
}

// 短信服务参数
type SMSConfig struct {
	AccessKeyId     string
	AccessKeySecret string
	SignName        string
	Vendor          string `common:"供应商，默认为ali，可选：ali|jumeng"`
	//TemplateCode    string
}

// SendSMS
// SMSParam 内置模板
type SMSParam struct {
	Phone string `common:"手机号"`
	Data  string `common:"短信参数（占位符的内容）"`
}

const (
	Vendor_JUMENG = "jumeng" //聚梦短信服务
	Vendor_ALI    = "ali"    //阿里短信服务
)
