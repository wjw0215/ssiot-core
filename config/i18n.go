package config

type I18n struct {
	Path          string
	DefaultLocale string
}

var I18nConfig = new(I18n)
