package config

type Mqtt struct {
	Host    string
	Port    int64
	User    string
	Passwd  string
	Timeout int
}

var MqttConfig = new(Mqtt)
