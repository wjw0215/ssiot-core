package memory

import (
	"gitee.com/sansaniot/ssiot-core/config/tool/loader"
	"gitee.com/sansaniot/ssiot-core/config/tool/reader"
	"gitee.com/sansaniot/ssiot-core/config/tool/source"
)

// WithSource appends a source to list of sources
func WithSource(s source.Source) loader.Option {
	return func(o *loader.Options) {
		o.Source = append(o.Source, s)
	}
}

// WithReader sets the config reader
func WithReader(r reader.Reader) loader.Option {
	return func(o *loader.Options) {
		o.Reader = r
	}
}
