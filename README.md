## 概述
核心库提供配置读取、数据库访问、缓存访问、日志、HTTP服务等基础组件

## 初始化
### 环境初始化
```go
env.SetupConfig("./settings.template.yml")
```
初始化环境之后即可通过env中的对象使用配置、数据库、缓存、日志等功能

### HTTP服务器启动
启动HTTP服务器，会自动进行环境初始化，不比再执行`env.SetupConfig()`方法，只需要执行：
```go
httpserver.InitEngine("./settings.template.yml")
```
HTTP服务器基于gin进行封装，关于如何使用路由、中间件请看下文中HTTP服务器部分。

## 配置
### 获取标准配置
标准配置是预先在核心库定义的一套固定格式的配置，可通过“标准配置对象”读取标准配置。如果不是按照标准配置格式配置的内容，通过“标准配置对象”获取不到。

````go
// 获取数据库连接配置
fmt.Prinln(env.DefaultConfig.Database.Source)
````
### 按路径获取配置
标准配置或者用户在yml配置文件中添加的其它非标准配置，均可通过“配置操作对象”获取。

````go
// 获取数据库连接配置
env.ConfigOperator.Get("ssiot.database.source")
````

## 数据库
### 使用DB对象
核心库根据配置文件中的配置初始化“Db对象”，“Db对象”是一个gorm对象，gorm的标准接口均可使用
````go
// 查询数据表
env.Db.Table("sys_svc").Select("instance_id, path, bin, arg").Rows()
````
### 使用gin上下文中的DB对象
如果启动了HTTP服务，核心库会自动将“Db对象”注入到gin的contex中，可在context中获取并使用
````go
 // 在gin上下文中获取db对象
 db := c.Get("db")
 db.Table("sys_svc").Select("instance_id, path, bin, arg").Rows()
````

## 缓存
如果配置文件中配置了redis节点，“缓存对象”使用redis管理缓存；如果没有配置redis节点，缓存对象使用内存管理缓存，支持线程安全。
````go
 // 设置一个100s过期的缓存并获取
 cache := env.Cache
 cache.Set("token", "mytoken", 100)
 cache.Get("token")
````

## 日志
核心库根据配置文件初始化“日志对象”。如果“driver”为“default”，“日志对象”使用标准库作为底层日志库，如果“driver”为“zap”，日志对象使用“zap”作为底层日志库。
````go
 // 设置一个100s过期的缓存并获取
 myLog := env.Log
 h1 := myLog.WithFields(map[string]interface{}{"key1": "val1"})
 h1.Trace("trace_msg1")
 h1.Warn("warn_msg1")
 h1.Infof("Info message %s", message)
````

## HTTP服务器
启动HTTP服务器非常简单，并且可以设置自定义路由和中间件。
````go
// 如有需要，向内核注册自己需要的中间件，自定义的中间件不在核心库中
// 可选 限流
httpserver.RegisterMiddlewareFunction(middleware.Sentinel())
// 可选 自动增加requestId
httpserver.RegisterMiddlewareFunction(middleware.RequestId(pkg.TrafficKey))
// 可选 请求日志记录到数据库
httpserver.RegisterMiddlewareFunction(middleware.LoggerToFile())
// 可选 自定义错误处理
httpserver.RegisterMiddlewareFunction(middleware.CustomError)
// 可选 http header中的缓存设置
httpserver.RegisterMiddlewareFunction(middleware.NoCache)
// 可选 跨域处理
httpserver.RegisterMiddlewareFunction(middleware.Options)
// 可选 http header中的安全设置
httpserver.RegisterMiddlewareFunction(middleware.Secure)

// 必须，初始化引擎
httpserver.InitEngine(_var.ConfigFile)
// 可选，初始化JWT验证方法组，JWT的部分回调可以自定义，
// 回调方法在ssiot-device项目的common目录中可以找到
jwtMiddleware, _ := middleware.AuthInit()
// 必须，向内核注册自己的业务模块路由，如不使用中间件，请使用InitRouters方法
httpserver.InitAuthRouters(Routers, jwtMiddleware)
// 必须，启动HTTP服务器
_ = httpserver.Run()
````

路由定义方式如下：
````go
func Routers(r *gin.RouterGroup, authMiddleware *jwt.GinJWTMiddleware) {
	router := r.Group("/auth")
	{
		router.POST("/modify", handler)
		router.POST("/delete", handler)
		router.GET("/get", handler)
	}
}
````
