package user

import (
	jwt "gitee.com/sansaniot/ssiot-core/httpmvc/jwtauth"
	"github.com/gin-gonic/gin"
)

func ExtractClaims(c *gin.Context) jwt.MapClaims {
	claims, exists := c.Get(jwt.JwtPayloadKey)
	if !exists {
		return make(jwt.MapClaims)
	}

	return claims.(jwt.MapClaims)
}

func Get(c *gin.Context, key string) interface{} {
	userInfo, _ := c.Get("user")
	userInfoMap := userInfo.(map[string]interface{})

	if userInfoMap == nil {
		//return nil
		// 防止服务报空异常
		return ""
	}

	return userInfoMap[key]
}
