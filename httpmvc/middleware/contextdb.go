package middleware

import (
	"gitee.com/sansaniot/ssiot-core/facade/runtime"
	"github.com/gin-gonic/gin"
)

func WithContextDb(c *gin.Context) {
	c.Set("db", runtime.Runtime.GetDbByKey(c.Request.Host).WithContext(c))
	c.Next()
}
