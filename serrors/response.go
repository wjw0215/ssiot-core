package serrors

type Response struct {
	Code      int         `json:"code" example:"200"`
	Message   interface{} `json:"message"`
	Result    interface{} `json:"result"`
	Success   bool        `json:"success"  example:"true"`
	Timestamp int64       `json:"timestamp"`
}
