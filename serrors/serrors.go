package serrors

import (
	"fmt"
	"gitee.com/sansaniot/ssiot-core/storage/i18n"
)

// 错误代码
const (
	OK                = 200
	Fail              = 201
	InvalidParam      = 202
	WrongUser         = 203
	WrongPassword     = 204
	WrongPhoneNumber  = 205
	InsertFail        = 206
	UpdateFail        = 207
	DeleteFail        = 208
	ModifyFail        = 209
	UploadFail        = 210
	SelectFail        = 211
	ProcessCreateFail = 300
	ProcessStartFail  = 301
	ProcessDuplicate  = 302
	ProcessUnexist    = 303
	ProcessDelFail    = 304
	ProcessFileLack   = 305
)

// 错误消息

var code2msg = map[int]string{
	OK:                "core_serrors_OK",
	Fail:              "core_serrors_Fail",
	InvalidParam:      "core_serrors_InvalidParam",
	WrongUser:         "core_serrors_WrongUser",
	WrongPassword:     "core_serrors_WrongPassword",
	InsertFail:        "core_serrors_InsertFail",
	UpdateFail:        "core_serrors_UpdateFail",
	DeleteFail:        "core_serrors_DeleteFail",
	ModifyFail:        "core_serrors_ModifyFail",
	SelectFail:        "core_serrors_SelectFail",
	UploadFail:        "core_serrors_UploadFail",
	WrongPhoneNumber:  "core_serrors_WrongPhoneNumber",
	ProcessCreateFail: "core_serrors_ProcessCreateFail",
	ProcessStartFail:  "core_serrors_ProcessStartFail",
	ProcessDuplicate:  "core_serrors_ProcessDuplicate",
	ProcessUnexist:    "core_serrors_ProcessUnexist",
	ProcessDelFail:    "core_serrors_ProcessDelFail",
	ProcessFileLack:   "core_serrors_ProcessFileLack",
}

func Msg(code int, lang ...string) string {
	var lg = ""
	if len(lang) > 0 && len(lang[0]) > 0 {
		lg = lang[0]
	}
	return i18n.T(lg, code2msg[code])
}

// SError 三三物联网Error定义
type SError struct {
	code int
	msg  string
}

func NewSError(code int) *SError {
	return &SError{
		code,
		code2msg[code],
	}
}

func (e *SError) Error() string {
	return fmt.Sprintf("Error: %+v %+v ", e.code, e.msg)
}

func (e *SError) Code() int {
	return e.code
}

func (e *SError) Msg() string {
	return e.msg
}

func (e *SError) Equal(errorCode int) bool {
	if e.Code() == errorCode {
		return true
	}

	return false
}
