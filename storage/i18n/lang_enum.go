package i18n

type LangCode string

const (
	LangChinese  LangCode = "zh-CN"
	LangEnglish  LangCode = "en-US"
	LangRussian  LangCode = "ru-RU"
	LangJapanese LangCode = "ja-JP"
	LangFrench   LangCode = "fr-FR"
)

var validLangCode = map[LangCode]bool{
	LangChinese:  true,
	LangEnglish:  true,
	LangRussian:  true,
	LangJapanese: true,
	LangFrench:   true,
}

func (s LangCode) String() string {
	return string(s)
}

func LangOf(code string) LangCode {
	langC := LangCode(code)
	if _, ok := validLangCode[langC]; !ok {
		return LangChinese
	}
	return langC
}
