package storage

import (
	"time"

	"github.com/bsm/redislock"
)

const (
	PrefixKey = "__host"
)

type AdapterCache interface {
	String() string
	Get(key string) (string, error)
	Set(key string, val interface{}, expire int) error
	HashSet(key string, val ...interface{}) error
	Del(key string) error
	HashGet(hk, key string) (string, error)
	HashGetAll(hk string) (map[string]string, error)
	HashDel(hk, key string) error
	HExists(hk, key string) (bool, error)
	Increase(key string) error
	Decrease(key string) error
	Expire(key string, dur time.Duration) error

	HIncrBy(key, field string, increment int) (int64, error)
	ScanKey(key string) []string
	LPush(key string, values ...interface{}) (int64, error)
	RPush(key string, values ...interface{}) error
	BRPop(timeout time.Duration, keys ...string) ([]string, error)
	RPop(key string) (string, error)
	LRange(key string, start, stop int64) ([]string, error)
}

type AdapterQueue interface {
	String() string
	Append(message Messager) error
	Register(name string, f ConsumerFunc)
	Run()
	Shutdown()
}

type Messager interface {
	SetID(string)
	SetStream(string)
	SetValues(map[string]interface{})
	GetID() string
	GetStream() string
	GetValues() map[string]interface{}
	GetPrefix() string
	SetPrefix(string)
}

type ConsumerFunc func(Messager) error

type AdapterLocker interface {
	String() string
	Lock(key string, ttl int64, options *redislock.Options) (*redislock.Lock, error)
}
